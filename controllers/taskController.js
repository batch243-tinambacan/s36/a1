const Task = require("../models/task");

// Controller function to get all the tasks
	module.exports.getAllTasks = () => {

		return Task.find({}).then(result =>{
			return result
		})
	}


// Controller function for creating a task
	module.exports.createTask = (requestBody) =>{
		
		let newTask = new Task({
			name: requestBody.name,
			status: requestBody.status
		})

		return newTask.save().then((task,error) =>{

			if (error){
				console.log(error);
				return false
			}
			else {
				return task
			}
		})
		}

// Controller function for getting a task
	module.exports.getTask = (taskId) => {
			return Task.findById(taskId).then((getTask,err) =>{

				if(err){
					console.log(err)
					return false;
				}
				else {
					return getTask
				}
			})
		}


// Controller function for updating a task
	module.exports.updateTask =(taskId) =>{

					return Task.findById(taskId).then((result, error) =>{
						if(error){
							console.log(error)
							return false
						}
							result.status = "completed";

					return result.save().then((updatedTask, saveErr) =>{

								if(saveErr){
									console.log(saveErr)
									return false
								}else{

									return updatedTask
								}
							})		
				})
			}

