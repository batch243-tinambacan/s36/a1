const express = require('express')

const router = express.Router();

const taskController = require("../controllers/taskController");

//Route to get all the tasks 
	router.get("/",(req,res)=>{
		taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
	})

// [Route to create new task]
	router.post("/",(req,res)=>{
			taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
		})

// [Route to get a task by ID]
	router.get("/:id", (req,res) => {
			taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController))
		})


// [Route to update task]
	router.put("/:id/complete",(req,res) =>{

		taskController.updateTask(req.params.id, req.body).then(
			resultFromController => res.send(resultFromController))
	})


module.exports = router;