// Setup dependencies
	const express = require('express');
	const mongoose = require('mongoose');

// Create an app using express function
	const app = express();
	const port = 3000;

	const taskRoute = require("./routes/taskRoute");

	app.use (express.json());
	app.use(express.urlencoded({extended:true}));
	app.use("/tasks", taskRoute)

// Connecting to MOngoDB atlas
	mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.azu7fek.mongodb.net/B243-s36?retryWrites=true&w=majority",
		{ 
			useNewUrlParser : true,  
			useUnifiedTopology : true
		}
	);

	app.listen(port, () => console.log(`Server successfully running at port ${port}`))